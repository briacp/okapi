package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.xml.XMLFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class ResxXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_xml-resx";
	private static final String DIR_NAME = "/resx/";
	private static final List<String> EXTENSIONS = Arrays.asList(".resx");

	public ResxXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, XMLFilter::new);
	}

	@Test
	public void resxFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(CONFIG_ID, false, new FileComparator.XmlComparator());
	}
}
