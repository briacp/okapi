package net.sf.okapi.xliffcompare.integration;

import net.sf.okapi.common.integration.XliffCompareIT;
import net.sf.okapi.common.integration.FileComparator;
import net.sf.okapi.filters.tex.TEXFilter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

@RunWith(JUnit4.class)
public class TexXliffCompareIT extends XliffCompareIT {
	private static final String CONFIG_ID = "okf_tex";
	private static final String DIR_NAME = "/tex/";
	private static final List<String> EXTENSIONS = Arrays.asList(".tex");

	public TexXliffCompareIT() {
		super(CONFIG_ID, DIR_NAME, EXTENSIONS, TEXFilter::new);
	}

	@Test
	public void texXliffCompareFiles() throws FileNotFoundException, URISyntaxException {
		realTestFiles(false, new FileComparator.XmlComparator());
	}
}
