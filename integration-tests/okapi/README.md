# Okapi Integration Tests

All long-running tests for Okapi should go here. Normally round trip tests or any tests that process full files.

**WARNING**

The `logback-test.xml` configuration file in `src\test\resources\` reduces the log level of `net.sf.okapi.common.pipelinedriver` to `WARN`. \
Without it the continuous integration build fails. The log limit is 4MB, and we can't change it (controlled by GitLab) \
Please don't change the level, and don't remove the file. If you do it in your local copy please don't commit it.

**Testing Goals**

The primary goal for the tests is to simulate a normal translation pripeline: **extract->segment->xliff->merge**. The 
original file is compared with the merged file and any differences that are problematic are logged. Code 
simplification is tested with **extract->segment->code simplify->xliff->merge**. The simplified merged file is 
compared with the merged file without simplification.

To protect against breaking changes in the filters the generated xliff is compared against a stored copy.

**Test File Directory Structure:**

Files in the root directory are processed with the default parameters of the filter. Each sub-directory is processed 
using the optional custom config files. Following the naming convention above is important for the test code to auto-detect 
the config files. Also be mindful that the tests include only the file extensions for your test files.

Only a single level of sub-directory is supported. Sub-directories within a sub-directory will be ignored. 

Bilingual file's locales are auto-detected.

**Example Test File Directory Structure:**

```
xmlstream
   normal.xml
   pcdata_tests
      okf_xmlstream@pcdata.fprm
      okf_html@pcdata.secondary.fprm
      pcdata.xml
```

These conventions allow large numbers of files and configurations to be tested with standard code. Any files that are 
added to `integration-tests/okapi/src/test/resources` will be automatically loaded and run by the integration tests.

Test files that fail should be excluded by adding the line `addKnownFailingFile("failed_file.ext");` in the test 
class contructor. These files are still run but do not count toward a test failure. If a `addKnownFailingFile` is 
added it is very likely there is a bug. These should be reconciled ASAP.  

How to update the xliff 1.2 golden files for the `net.sf.okapi.xliffcompare.integration` tests:

1. Run the all test in `net.sf.okapi.roundtrip.integration`. Be sure to enable the option to 
   force update of snaphots so that the latest local Okapi code is included.
3. Navigate to `$okapi_root/integration-tests/okapi/target/test-classes/..` and select the newly generated xliff file(s).
   Be sure to also copy over the ones in sub-directories.
4. Copy the xliff files to the corresponding directories under 
   `$okapi_root/integration-tests/okapi/src/test/resources/XLIFF_PREV`
5. Rerun `net.sf.okapi.xliffcompare.integration` to make sure all tests pass.
