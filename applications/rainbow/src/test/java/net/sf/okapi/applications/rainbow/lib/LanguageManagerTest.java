package net.sf.okapi.applications.rainbow.lib;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class LanguageManagerTest {

	@Test
	public void test() {
		// Minimal testing
		LanguageManager lm = new LanguageManager();
		assertNotEquals(0, lm.getCount());

		assertNotEquals(-1, lm.getIndexFromCode("zh"));
		assertNotEquals(-1, lm.getIndexFromCode("zh-CN"));
		assertNotEquals(-1, lm.getIndexFromCode("zh-Hans"));
		assertNotEquals(-1, lm.getIndexFromCode("zh-Hans-CN"));

		assertNotEquals(-1, lm.getIndexFromCode("zh"));
		assertNotEquals(-1, lm.getIndexFromCode("zh-TW"));
		assertNotEquals(-1, lm.getIndexFromCode("zh-Hant"));
		assertNotEquals(-1, lm.getIndexFromCode("zh-Hant-TW"));

		assertEquals(-1, lm.getIndexFromCode("zh-Hans-TW"));

		assertEquals("French", lm.GetNameFromCode("fr"));
		LanguageItem li = lm.GetItem("fr");
		assertEquals("French", li.name);
		assertEquals("fr", li.code);
	}
}
