/*
 * =============================================================================
 *   Copyright (C) 2010-2017 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.idml;

import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.List;

class StoryChildElementsWriter {

    private final StyleRangeEventsGenerator styleRangeEventsGenerator;
    private List<XMLEvent> events;
    private StyleRanges currentStyleRanges;

    StoryChildElementsWriter(StyleRangeEventsGenerator styleRangeEventsGenerator) {
        this.styleRangeEventsGenerator = styleRangeEventsGenerator;
    }

    List<XMLEvent> write(List<StoryChildElement> storyChildElements) {
        this.events = new ArrayList<>();
        for (StoryChildElement storyChildElement : storyChildElements) {
            if (!(storyChildElement instanceof StoryChildElement.StyledTextElement)) {
                writeAsNonStyledTextElement(storyChildElement);
                continue;
            }
            writeAsStyledTextElement(storyChildElement);
        }
        return this.events;
    }

    private void writeAsNonStyledTextElement(final StoryChildElement storyChildElement) {
        if (null != currentStyleRanges) {
            writeClosingStyleRanges();
        }
        this.events.addAll(storyChildElement.getEvents());
        if (null != currentStyleRanges) {
            writeOpeningStyleRanges();
        }
    }

    private void writeAsStyledTextElement(final StoryChildElement storyChildElement) {
        if (null == currentStyleRanges) {
            this.currentStyleRanges = ((StoryChildElement.StyledTextElement) storyChildElement).getStyleRanges();
            writeOpeningStyleRanges();
        }
        StyleRanges styleRanges = ((StoryChildElement.StyledTextElement) storyChildElement).getStyleRanges();
        if (!currentStyleRanges.getParagraphStyleRange().equals(styleRanges.getParagraphStyleRange())) {
            writeClosingStyleRanges();
            currentStyleRanges = styleRanges;
            writeOpeningStyleRanges();
        } else if (!currentStyleRanges.getCharacterStyleRange().equals(styleRanges.getCharacterStyleRange())) {
            this.events.addAll(styleRangeEventsGenerator.generateCharacterStyleRangeEnd());
            currentStyleRanges = styleRanges;
            this.events.addAll(styleRangeEventsGenerator.generateCharacterStyleRangeStart(currentStyleRanges));
        }
        this.events.addAll(storyChildElement.getEvents());
    }

    private void writeOpeningStyleRanges() {
        this.events.addAll(styleRangeEventsGenerator.generateParagraphStyleRangeStart(currentStyleRanges));
        this.events.addAll(styleRangeEventsGenerator.generateCharacterStyleRangeStart(currentStyleRanges));
    }

    private void writeClosingStyleRanges() {
        this.events.addAll(styleRangeEventsGenerator.generateCharacterStyleRangeEnd());
        this.events.addAll(styleRangeEventsGenerator.generateParagraphStyleRangeEnd());
    }
}
