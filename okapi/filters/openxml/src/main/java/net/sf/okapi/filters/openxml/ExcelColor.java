/*
 * =============================================================================
 * Copyright (C) 2010-2021 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Iterator;
import java.util.LinkedList;

interface ExcelColor {
    String BACKGROUND = "bgColor";
    String FOREGROUND = "fgColor";
    String argb();
    boolean themeAvailable();
    int themeId();
    void readWith(final XMLEventReader reader) throws XMLStreamException;
    Markup asMarkup();

    final class Empty implements ExcelColor {
        private static final String EMPTY = "";

        @Override
        public String argb() {
            return EMPTY;
        }

        @Override
        public boolean themeAvailable() {
            return false;
        }

        @Override
        public int themeId() {
            return 0;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
        }

        @Override
        public Markup asMarkup() {
            return new Markup.Empty();
        }
    }

    final class Default implements ExcelColor {
        public static final String INDEXED = "indexed";
        public static final String RGB = "rgb";
        public static final String THEME = "theme";
        private final StartElement startElement;
        private boolean idAvailable;
        private int id;
        private String argb;
        private boolean themeIdAvailable;
        private int themeId;
        private EndElement endElement;
        private boolean startElementAttributesRead;

        Default(final StartElement startElement) {
            this.startElement = startElement;
        }

        @Override
        public String argb() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.argb;
        }

        @Override
        public boolean themeAvailable() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.themeIdAvailable;
        }

        @Override
        public int themeId() {
            if (!this.startElementAttributesRead) {
                readStartElementAttributes();
            }
            return this.themeId;
        }

        private void readStartElementAttributes() {
            final Iterator iterator = this.startElement.getAttributes();
            while (iterator.hasNext()) {
                final Attribute a = (Attribute) iterator.next();
                switch (a.getName().getLocalPart()) {
                    case INDEXED:
                        this.idAvailable = true;
                        this.id = Integer.parseUnsignedInt(a.getValue());
                        break;
                    case RGB:
                        this.argb = a.getValue();
                        break;
                    case THEME:
                        this.themeIdAvailable = true;
                        this.themeId = Integer.parseUnsignedInt(a.getValue());
                        break;
                }
            }
            this.startElementAttributesRead = true;
        }

        @Override
        public void readWith(final XMLEventReader reader) throws XMLStreamException {
            while (reader.hasNext()) {
                final XMLEvent e = reader.nextEvent();
                if (e.isEndElement() && e.asEndElement().getName().equals(this.startElement.getName())) {
                    this.endElement = e.asEndElement();
                    break;
                }
            }
        }

        @Override
        public Markup asMarkup() {
            if (null == this.endElement) {
                throw new IllegalStateException("The end element is not available");
            }
            final MarkupBuilder mb = new MarkupBuilder(new Markup.General(new LinkedList<>()));
            mb.add(this.startElement);
            mb.add(this.endElement);
            return mb.build();
        }
    }
}
