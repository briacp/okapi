/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

interface RevisionLogFragments {
    Set<String> revisionIds();
    void readWith(final XMLEventReader eventReader) throws XMLStreamException;

    final class Default implements RevisionLogFragments {
        private static final Set<String> reviewableRevisions = new HashSet<>(
            Arrays.asList(
                "rrc", "rm", "rsnm", "ris", "rcc", "rcft"
                // The "raf", "rcmt", "rcv", "rdn", "rfmt", "rqt" revisions are non-reviewable
            )
        );
        private static final QName R_ID = new QName("rId");
        private final Set<String> revisionIds;

        Default() {
            this(new HashSet<>());
        }

        Default(final Set<String> revisionIds) {
            this.revisionIds = revisionIds;
        }

        @Override
        public Set<String> revisionIds() {
            return this.revisionIds;
        }

        @Override
        public void readWith(final XMLEventReader eventReader) throws XMLStreamException {
            while (eventReader.hasNext()) {
                final XMLEvent e = eventReader.nextEvent();
                if (!e.isStartElement()) {
                    continue;
                }
                final StartElement se = e.asStartElement();
                if (Default.reviewableRevisions.contains(se.getName().getLocalPart())) {
                    this.revisionIds.add(XMLEventHelpers.getAttributeValue(se, R_ID));
                }
            }
        }
    }
}
