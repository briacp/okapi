/*===========================================================================
  Copyright (C) 2018 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/
package net.sf.okapi.common.filters;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.EventType;
import net.sf.okapi.common.ReversedIterator;
import net.sf.okapi.common.resource.Code;
import net.sf.okapi.common.resource.ITextUnit;
import net.sf.okapi.common.resource.TextFragment;
import net.sf.okapi.common.resource.TextPart;
import org.xmlunit.builder.DiffBuilder;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.ComparisonControllers;
import org.xmlunit.diff.Diff;
import org.xmlunit.diff.DifferenceEvaluator;
import org.xmlunit.diff.DifferenceEvaluators;

import javax.xml.parsers.ParserConfigurationException;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Convenient methods for filter unit tests.
 *
 */
public class FilterTestUtil {
    /**
     * Asserts that the given string is found in a translatable Text Unit in the list.
     * @param tus List of Text Units
     * @param expectedString The string that should be found in a translatable TU
     */
    public static void assertTUListContains(List<ITextUnit> tus, String expectedString) {
        for(ITextUnit tu: tus) {
            if (tu.isTranslatable()) {
                for(TextPart tp: tu.getSource().getParts()) {
                    if (tp.text.getText().contains(expectedString)) {
                        return;
                    }
                }
            }
        }
        fail(String.format("Some Text Unit should contain \"%s\" in their translatable parts." , expectedString));
    }

    /**
     * Asserts that the given string is not found in any translatable Text Unit in the list.
     * @param tus List of Text Units
     * @param excludedString The string that should not be found
     */
    public static void assertTUListDoesNotContain(List<ITextUnit> tus, String excludedString) {
        for(ITextUnit tu: tus) {
            if (tu.isTranslatable()) {
                for(TextPart tp: tu.getSource().getParts()) {
                    if (tp.text.getText().contains(excludedString)) {
                        fail(String.format("No Text Unit should NOT contain \"%s\" in their translatable parts" , excludedString));
                    }
                }
            }
        }
    }

    /**
     * Asserts that the event is a translatable Text Unit of the given string and codes.
     * @param event The Event that should be a Text Unit
     * @param content The string that the Text Unit's toString() is expected to return
     * @param codes The list of expected codes
     */
    public static void assertTextUnit(Event event, String content, String... codes) {
        assertEquals(EventType.TEXT_UNIT, event.getEventType());
        assertTrue(event.getTextUnit().isTranslatable());
        assertTextUnit(event.getTextUnit(), content, codes);
    }

    /**
     * Asserts that the Text Unit is of the given string and codes.
     * @param tu The Text Unit to test
     * @param content The string that the Text Unit's toString() is expected to return
     * @param codes The list of expected codes
     */
    public static void assertTextUnit(ITextUnit tu, String content, String... codes) {
        assertEquals("text unit content", content, tu.toString());
        assertEquals("#codes", codes.length, getNumCodes(tu));
        for (int i = 0; i < codes.length; i++) {
            assertEquals("code#" + i, codes[i], getCodeString(tu, i));
        }
    }

    private static int getNumCodes(ITextUnit tu) {
        return tu.getSource().getFirstContent().getCodes().size();
    }

    private static String getCodeString(ITextUnit tu, int index) {
        return tu.getSource().getFirstContent().getCodes().get(index).toString();
    }

    /**
     * Asserts the event is a document part with the given content.
     * @param event The Event that should be a Document Part
     * @param content The expected string of the Document Part
     */
    public static void assertDocumentPart(Event event, String content) {
        assertEquals(EventType.DOCUMENT_PART, event.getEventType());
        assertEquals(content, event.getDocumentPart().toString());
    }

    /**
     * Compare two xml files and return their differences if any
     * @param <T> The input type. Can be a string path, Stream or any number of input types
     *
     * @param actual the generated file (normally merged)
     * @param expected the original file
     * @param diffEvaluators custom {@link DifferenceEvaluator}
     * @return the {@link Diff} object with the differences if any
     * @throws ParserConfigurationException
     */
    public static <T> Diff compareXml(final T actual, final T expected,
                                      boolean ignoreWhitespace,
                                      DifferenceEvaluator... diffEvaluators) throws ParserConfigurationException {
        // DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // FIXME: ideally we should have this set - but not all files pass:
        // dbf.setValidating(true);

        // combine default and custom DifferenceEvaluators
        List<DifferenceEvaluator> evs = new LinkedList<>();
        evs.add(DifferenceEvaluators.Default);
        evs.add(DifferenceEvaluators.ignorePrologDifferences());
        for (DifferenceEvaluator ev: diffEvaluators) {
            evs.add(ev);
        }
        DifferenceEvaluator[] evsArray = new DifferenceEvaluator[evs.size()];
        final DiffBuilder diff = DiffBuilder.compare(Input.from(expected)).withTest(Input.from(actual))
                .checkForSimilar()
                .ignoreComments()
                .withComparisonController(ComparisonControllers.StopWhenDifferent)
                .withAttributeFilter(new XmlStandardAttributesToIgnore())
                .withDifferenceEvaluator(DifferenceEvaluators.chain(evs.toArray(evsArray)));
        if (ignoreWhitespace) {
            diff.ignoreWhitespace().normalizeWhitespace();
        }
        return diff.build();
    }

    /**
     * UNIMPLEMENTED!!
     * Reverse the codes in the fragment while maintaining wellformedness.
     * @param tf fragment that will have it's codes reversed
     * @return new fragment with reversed codes
     */
    public static TextFragment reverseCodes(TextFragment tf) {
        if (tf == null || tf.isEmpty() || !tf.hasCode() || tf.getCodes().size() <= 1) {
            return tf;
        }

        Stack<Integer> idStack = new Stack<>();
        idStack.push(-1);
        List<Code> codes = tf.getCodes();

        // Process the markers
        for ( int i=0; i<tf.length(); i++ ) {
            switch ( tf.charAt(i) ) {
                case TextFragment.MARKER_OPENING:
                case TextFragment.MARKER_CLOSING:
                case TextFragment.MARKER_ISOLATED:
                    int index = TextFragment.toIndex(tf.charAt(i + 1));
                    Code code = codes.get(index);
                    if (code.getTagType() == TextFragment.TagType.OPENING) {
                        idStack.push(index);
                    }
                    i++; // Skip index part of the code marker
            }
        }

        return reverse(tf, idStack);
    }

    /**
     * UNIMPLEMENTED!!
     * @param tf
     * @param idStack
     * @return
     */
    private static TextFragment reverse(TextFragment tf, Stack<Integer> idStack) {
        TextFragment reversed = new TextFragment(tf.toString());

        // placeholders only
        if (idStack.isEmpty()) {
            for (Code c: ReversedIterator.reverse(tf.getCodes())) {
            }
        }
        return reversed;
    }
}
