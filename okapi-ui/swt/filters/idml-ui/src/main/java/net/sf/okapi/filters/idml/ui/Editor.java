/*
 * =============================================================================
 * Copyright (C) 2010-2022 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.idml.ui;

import net.sf.okapi.common.EditorFor;
import net.sf.okapi.common.IContext;
import net.sf.okapi.common.IHelp;
import net.sf.okapi.common.IParameters;
import net.sf.okapi.common.IParametersEditor;
import net.sf.okapi.common.ui.Dialogs;
import net.sf.okapi.common.ui.OKCancelPanel;
import net.sf.okapi.common.ui.UIUtil;
import net.sf.okapi.filters.idml.Parameters;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import java.util.ResourceBundle;

@EditorFor(Parameters.class)
public class Editor implements IParametersEditor {
    private ResourceBundle resourceBundle;
    private Parameters params;
    private boolean readOnly;
    private IHelp help;
    private Shell shell;
    private Inputs inputs;
    private boolean edited;

    @Override
    public boolean edit(final IParameters paramsObject, final boolean readOnly, final IContext context) {
        this.resourceBundle = ResourceBundle.getBundle("net.sf.okapi.filters.idml.ui.Editor");
        this.params = (Parameters) paramsObject;
        this.readOnly = readOnly;
        this.help = (IHelp) context.getObject("help");
        this.edited = false;
        try {
            this.shell = shellWith((Shell) context.getObject("shell"));
            configureInputs();
            configureActions();
            configureGeneralListeners();
            this.shell.pack();
            this.shell.open();
            final Display display = this.shell.getShell().getDisplay();
            while (!this.shell.isDisposed()) {
                if (!display.readAndDispatch()) {
                    display.sleep();
                }
            }
        } catch (Exception e) {
            Dialogs.showError(this.shell, e.getLocalizedMessage(), null);
        } finally {
            if (this.shell != null) {
                this.shell.dispose();
            }
        }
        return this.edited;
    }

    private Shell shellWith(final Shell parent) {
        final Shell shell = new Shell(parent, SWT.CLOSE | SWT.TITLE | SWT.RESIZE | SWT.APPLICATION_MODAL);
        shell.setText(this.resourceBundle.getString("idml-filter-parameters"));
        if (parent != null) {
            UIUtil.inheritIcon(shell, parent);
        }
        shell.setLayout(new GridLayout());
        return shell;
    }

    private void configureInputs() {
        this.inputs = new Inputs.Default(
            ResourceBundle.getBundle("net.sf.okapi.filters.idml.ui.Inputs"),
            this.shell
        );
        this.inputs.configureFrom(this.params);
    }

    private void configureActions() {
        final SelectionAdapter actionsHandler = new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent e) {
                if ("h".equals(e.widget.getData())) {
                    if (help != null) {
                        help.showWiki("IDML Filter");
                    }
                } else if ("o".equals(e.widget.getData())) {
                    params.reset();
                    try {
                        inputs.saveTo(params);
                        edited = true;
                        shell.close();
                    } catch (final Exception ex) {
                        Dialogs.showError(shell, ex.getLocalizedMessage(), null);
                    }
                } else {
                    shell.close();
                }
            }
        };
        final OKCancelPanel actionsPanel = new OKCancelPanel(this.shell, SWT.NONE, actionsHandler, true);
        actionsPanel.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
        actionsPanel.btOK.setEnabled(!this.readOnly);
        if (!this.readOnly) {
            this.shell.setDefaultButton(actionsPanel.btOK);
        }
    }

    private void configureGeneralListeners() {
        this.shell.addListener(SWT.Traverse, event -> {
            if (event.detail == SWT.TRAVERSE_ESCAPE) {
                this.shell.close();
            }
        });
    }

    @Override
    public IParameters createParameters() {
        return new Parameters();
    }
}
